"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""
import tkinter as tk
import Pmw
import os
from tkinter import messagebox
attach_no=0

#fetch Details from server about the SCAN_ID
def getdetails(scan_id):
	return "\nThis data is feched from the server based on the"+"\nscan_id:"+scan_id+"\n\nEdit getdetails(scan_id) function to modify it";

#upload files to the server
def upload_files(attach):
	return 1
	
	
class scannerapp(tk.Tk):
    def __init__(self,parent):
        tk.Tk.__init__(self,parent)
        self.parent=parent
        self.resizable(width=False, height=False)
        self.file_id=tk.StringVar()
        self.attachments=[]
        self.draw_frame()
        self.initialize()
    def initialize(self):
        self.load_default()
        self.draw_frame1()
    def load_default(self):
        self.file_id.set(u'10101');
        self.attachments=[]
    def draw_frame(self):
        self.f=tk.Frame(self,bg="blue",width=800,height=500)
        self.f.pack(side=tk.LEFT,expand=2,fill=tk.BOTH)

        self.f1=tk.Frame(self.f,bg="grey",width=800,height=500)
        self.f1.grid(column=0,row=0,padx=5,pady=5,sticky='EW')
    def draw_frame1(self):
        global attach_no
        attach_no=0
        self.label=tk.Label(self.f1,text="Enter the Scan ID")
        self.label.place(relx=0.5,rely=0.4,anchor=tk.CENTER)
        self.entry1=tk.Entry(self.f1,textvariable=self.file_id)
        self.entry1.place(height=35,relx=0.5,rely=0.5,anchor=tk.CENTER)
        
        self.submit_button=tk.Button(self.f1,text=u"Submit",command=self.onSubmit)
        self.submit_button.place(relx=0.5,rely=0.6,anchor=tk.CENTER)
    def get_file_info(self,file_id):
        details = getdetails(self.file_id.get());
        print (details)
        return "you have to replace it with the output returned"+details;
    def draw_frame2(self,output):
        text=Pmw.ScrolledText(self.f1,borderframe=5,vscrollmode='dynamic',labelpos='n',
                label_text='Output Info',text_width=60,text_height=10)
        text.place(relx=0.5,rely=0.5,anchor=tk.CENTER)
        text.insert('end',output)

        self.confirm_button=tk.Button(self.f1,text=u'Proceed to Scan',command=self.onProceed)
        self.confirm_button.place(relx=0.4,rely=0.7,anchor=tk.CENTER)
        self.cancel_button=tk.Button(self.f1,text=u'Cancel Scanning',command=self.onCancel)
        self.cancel_button.place(relx=0.6,rely=0.7,anchor=tk.CENTER)
    def draw_frame3(self):
    	text1=Pmw.ScrolledText(self.f1,borderframe=5,vscrollmode='dynamic',labelpos='n',
    	label_text='Uploading File Information', text_width=60,text_height=10)
    	text1.place(relx=0.5,rely=0.5,anchor=tk.CENTER)
    	text1.insert('end',self.attachments)
    	
    	self.upload_confirm=tk.Button(self.f1,text=u'Confirm To Upload',command=self.onConfirm)
    	self.upload_confirm.place(relx=0.4,rely=0.7,anchor=tk.CENTER)
    	self.upload_cancel=tk.Button(self.f1,text=u'Cancel Upload',command=self.onCancel)
    	self.upload_cancel.place(relx=0.6,rely=0.7,anchor=tk.CENTER)
    def onSubmit(self):
        print(self.file_id.get())
        #call the function when the user clicks the submit button
        result=self.get_file_info(self.file_id.get())
        self.f.destroy()
        self.draw_frame()
        self.draw_frame2(result)
    def onProceed(self):
        global attach_no
        file_name=self.file_id.get()+'_'+str(attach_no)+'.pdf'
        attach_no+=1
        result=self.start_scan(file_name);
        self.attachments.append(file_name)
        if(result==1):
            messagebox.showinfo("tk","successfully scanned")
            if messagebox.askyesno("tk","Do you have any other attachments?"):
                self.onProceed()
            else:
                self.onUpload()
    def onCancel(self):
        #self.frame
        self.load_default()
        self.f.destroy()
        self.draw_frame()
        self.draw_frame1()
    def onUpload(self):
    	self.f.destroy()
    	self.draw_frame()
    	self.draw_frame3()
    def onConfirm(self):
    	upload_files(self.attachments)
    	messagebox.showinfo("tk","successfully uploaded")
    	self.onCancel()
    def start_scan(self,file_name):
        os.system("./scan.sh "+file_name)
        return 1
    def onQuit(self):
        self.quit()

if __name__=="__main__":
    app=scannerapp(None)
    app.title('Bulk	ScannerApp')
    app.mainloop()
