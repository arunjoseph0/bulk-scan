#!/bin/bash
scanimage -d kds_i11xx:i1100 --batch=$pages_%05d.tiff
for img in *.tiff
do
	flag=$(python is_blank.py $img)
	if [ "$flag" = "1" ]
	then 
		echo rm $img
	fi
done
convert *.tiff $1
rm *.tiff
echo $1 Generated
